<?php

function atom_scripts(){
	
	global $wp_query;

	wp_enqueue_style('fancybox', get_template_directory_uri() . '/assets/@fancyapps/fancybox/dist/jquery.fancybox.min.css', array(), '', 'all');
	wp_enqueue_style('slick', get_template_directory_uri() . '/assets/slick-carousel/slick/slick.css', array(), '', 'all');


	wp_enqueue_style('fonts', 'https://fonts.googleapis.com/css?family=Material+Icons&subset=latin-ext', array(), '', 'all' );
	wp_enqueue_style('main', get_template_directory_uri() . '/css/main.css', array(), filemtime( get_template_directory() . '/css/main.css' ), 'all');

	wp_enqueue_script('libs', get_template_directory_uri() . '/js/libs.js', array(), filemtime( get_template_directory() . '/js/libs.js' ), true);
	
	wp_register_script('main', get_template_directory_uri() . '/js/app.js', array('jquery'), filemtime( get_template_directory() . '/js/app.js' ), true);
	wp_localize_script('main', 'wp_params', array(
	    'ajax_url' => admin_url( 'admin-ajax.php' ),
	    'posts' => json_encode( $wp_query->query_vars ), // everything about your loop is here
	    'current_page' => get_query_var( 'paged' ) ? get_query_var('paged') : 1,
	    'max_page' => $wp_query->max_num_pages,
	));
	wp_enqueue_script('main');
	
}
add_action('wp_enqueue_scripts', 'atom_scripts');



function atom_add_editor_styles() {
  add_editor_style( 'css/editor.css' );
}
add_action( 'admin_init', 'atom_add_editor_styles' );

function wpse33318_tiny_mce_before_init( $mce_init ) {
  $mce_init['cache_suffix'] = 'v=' . filemtime( get_template_directory() . '/css/editor.css' );
  return $mce_init;    
}
add_filter( 'tiny_mce_before_init', 'wpse33318_tiny_mce_before_init' );


function atom_add_admin_styles() {
	wp_enqueue_style('admin', get_template_directory_uri() . '/css/admin.css', array(), filemtime( get_template_directory() . '/css/admin.css' ), 'all');
}
add_action( 'admin_head', 'atom_add_admin_styles' );

